import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'model/item.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'todoApp',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green
      ),
      home: HomePage(),
    );
  }
  
}

class HomePage extends StatefulWidget {
  var items = new List<Item>();
  
  HomePage() {
    items = [];
    // items.add(Item(title: 'Acordar', done: true));
    // items.add(Item(title: 'Escovar os dentes', done: false));
    // items.add(Item(title: 'Tomar um cafe', done: true));
    // items.add(Item(title: 'Pegar o Onibus', done: false));
    // items.add(Item(title: 'Chegar no Trampo', done: false));
  }
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var newTaskCtrl = new TextEditingController();
  
  void add() {
    if (newTaskCtrl.text.isEmpty) return;

    setState(() {
      widget.items.add(Item(title: newTaskCtrl.text, done: false));
      save();
      newTaskCtrl.clear();
    });
  }

  void remove(int index) {
    setState(() => widget.items.removeAt(index));
    save();
  }
  
  void renderItems(List<Item> items) => setState(() => widget.items = items);
  
  Future load() async {
    var instance = await SharedPreferences.getInstance();
    var data = instance.getString('data');

    if (data != null) {
      Iterable decoded = jsonDecode(data);
      List<Item> result = decoded.map((x) => Item.fromJson(x)).toList();

      renderItems(result);
    }
  }

  save() async {
    var instance = await SharedPreferences.getInstance();
    await instance.setString('data', jsonEncode(widget.items));
  }

  _HomePageState() {
    load();
  }
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: newTaskCtrl,
          keyboardType: TextInputType.text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
          decoration: InputDecoration(
            labelText: 'New task',
            labelStyle: TextStyle(color: Colors.white)
          ),
        ),
      ),
      body : ListView.builder(
        itemCount: widget.items.length,
        itemBuilder: (BuildContext context, int index) {
          final item = widget.items[index];
          return Dismissible(
            child: CheckboxListTile(
              title: Text(item.title),
              value: item.done,
              onChanged: (value) {
                setState(() => item.done = value);
                save();
              },
            ),
            key: Key(item.title),
            background: Container(
              color: Colors.red.withOpacity(0.2),
            ),
            onDismissed: (direction) {
              remove(index);
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: add,
        child: Icon(Icons.add),
        backgroundColor: Colors.pink,
      ),
    );
  }
}
